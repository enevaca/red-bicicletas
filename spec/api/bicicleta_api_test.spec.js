var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');
const { base } = require('../../models/bicicleta');

var base_url = 'http://localhost:5000/api/bicicletas';

describe('Bicicleta API', () => {
  beforeAll((done) => { mongoose.connection.close(done) });

  beforeEach((done) => {
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      console.log('We are connected to test database!');
      done();
    });
  });

  afterEach((done) => {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      mongoose.disconnect();
      done();
    });
  });

	describe('GET BICICLETAS /', () => {
		it('Status 200', (done) => {
      request.get(base_url, (error, response, body) => {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
			});
		});
	});

	describe('POST BICICLETA /create', () => {
		it('STATUS 200', (done) => {
			var headers = { 'content-type': 'application/json' };
			var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';
			request.post({
				headers: headers,
				url: base_url + '/create',
				body: aBici
			}, (error, response, body) => {
          expect(response.statusCode).toBe(200);
          var bici = JSON.parse(body).bicicleta;
          console.log(bici);
          expect(bici.color).toBe('rojo');
          expect(bici.ubicacion[0]).toBe(-34);
          expect(bici.ubicacion[1]).toBe(-54);
					done();
			});
		});
	});

	describe('POST BICICLETA /update', () => {
		it('STATUS 202', (done) => {
			var a = new Bicicleta(10, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
			Bicicleta.add(a);

			var headers = { 'content-type': 'application/json' };
			var aBici = '{ "code": 10, "color": "verde", "modelo": "urbana", "lat": -34, "lng": -54 }';
			request.put({
				headers: headers,
				url: base_url + '/10/update',
				body: aBici
			}, (error, response, body) => {
				expect(response.statusCode).toBe(202);
					expect(Bicicleta.findByCode(10).color).toBe('verde');
					done();
			});
		});
	});

});